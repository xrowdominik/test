
FROM centos:7 
#FROM docker.io/centos:latest

MAINTAINER dominik@xrow.de

#RUN yum -y install patch git subversion python-pip redis ansible

# Ansible

#RUN echo t|svn --username bjoern --password Xrowpas2 --quiet --no-auth-cache export --force https://svn.xrow.net/svn/xrow/trunk/provision /root/provision
#ADD ansible.diff ansible.diff
#RUN patch -p 0 --batch -u --ignore-whitespace /usr/lib/python2.7/site-packages/ansible/modules/core/system/service.py
#RUN mkdir root/.ssh
#COPY ssh/id_rsa root/.ssh/id_rsa
#COPY ssh/known_hosts root/.ssh/known_hosts
#RUN chmod -R 600 /root/.ssh/id_rsa
#RUN ansible-galaxy install -f -r /root/provision/requirements.yml
#RUN ansible-playbook ~/provision/ezcluster.docker.yml -i ~/provision/hosts --connection=local
#RUN rm -Rf /root/provision
#RUN rm -Rf /etc/ansible/roles
COPY ezcluster /etc/ezcluster

#CMD ["/usr/sbin/init"]
# set default workdir
WORKDIR /root



#EXPOSE 5000

